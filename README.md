Scala sandbox
============


Install scala: `brew instal sbt`

Complie: `scalac hello.scala`

Run: `scala HelloWorld`



Setup emulator from command line
===============================


Add `$ANDROID_HOME/platform-tools` to the path.

Create a avd and start a emulator:

```
android list targets
android create avd -n my_android1.5 -t 2 --abi default/x86_64
emulator @my_android1.5
```

Install an APK: `adb install XXX.apk`


Android development
===================

android-sdk-plugin
------------------

https://github.com/pfn/android-sdk-plugin

Create project:

```
android create project -t 1 -k com.gizur.test2 -p test2 -a MainActivity
cd test2
sbt "gen-android-sbt"
```

Build with: `sbt compile`


Other tools
-----------


Second try:

Instal Scala plugin for Android Studio/Intellij

* https://github.com/yareally/android-scala-intellij-no-sbt-plugin


First try:

Install android plug by updating this file (see guide below): `~/.sbt/0.13/plugins/android.sbt`

Then do this in the project that should use scala: `android update project` (did not work)

Generate new project: `gen-android` (can't find this tool)

* Guide: http://macroid.github.io/ScalaOnAndroid.html
* Setup IDE: http://scala-ide.org/docs/tutorials/androiddevelopment/


Resources
---------

* https://github.com/macroid/macroid


